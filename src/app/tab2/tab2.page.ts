import { Component } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { actionSheetController } from '@ionic/core';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})

export class Tab2Page {

  emprestimo: any[] = [];
  constructor(private ActionSheetController : ActionSheetController){
     let emprestimoJson = localStorage.getItem('equipsDb');
     if (emprestimoJson != null){
       this.emprestimo = JSON.parse(emprestimoJson);
     }
  }
  
  excluir(equip: any){
    this.emprestimo = this.emprestimo.filter(emprestimoArray => equip != emprestimoArray);
    this.updateLocalStorage();
  }

  updateLocalStorage(){
    localStorage.setItem('equipsDb', JSON.stringify(this.emprestimo));
  }

}
