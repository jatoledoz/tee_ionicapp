import { Component } from '@angular/core';
import { AlertController, ToastController, ActionSheetController } from '@ionic/angular';
import { actionSheetController } from '@ionic/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page {

  equips: any[] = [];
  emprestimo: any[] = [];
  constructor(private AlertController: AlertController,
     private ToastController : ToastController,
     private ActionSheetController : ActionSheetController){
      let equipsJson = localStorage.getItem('equipsDb');
      if (equipsJson != null){
        this.equips = JSON.parse(equipsJson);
      }

     }

  async showAdd() {
    const alert = await this.AlertController.create({
      header: 'Cadastro de Equipamento',
      inputs: [
        {
          name: 'equip',
          type: 'text',
          placeholder: 'Equipamento'
        },{
          name: 'qtd',
          type: 'number',
          min: 1,
          max: 20,
          placeholder: 'Quantidade'
        },
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Adicionar',
          handler: (form) => {
            this.add(form.equip);
            this.add(form.qtd);
          }
        }
      ]
    });

    await alert.present();
  }

  async add(equip : string){
    if (equip.trim().length <1){
      const toast = await this.ToastController.create({
        message: 'Informe o nome do equipamento',
        duration: 2000,
        position: 'top'
      });
      toast.present();
      return;
    }
    let equips = {name: equip, qtd: equip};
    this.equips.push(equips);
  }

  async alugar(equip: any){
    const ActionSheet = await this.ActionSheetController.create({
      header: "O que deseja fazer?",
      buttons: [{
        text: 'Alugar',
        icon: 'bookmark',
        handler: () =>{
          let emprestimo = {name: equip, qtd: equip}
          this.updateLocalStorage();
        }
      },{
        text: 'Cancelar',
        icon: 'close',
        role: 'cancel',
        handler: () =>{
          console.log('Cancelado');
        }
      }]
    });
    await ActionSheet.present();
  }
  excluir(equip: any){
    this.equips = this.equips.filter(equipsArray => equip != equipsArray);
    this.updateLocalStorage();
  }

  updateLocalStorage(){
    localStorage.setItem('equipsDb', JSON.stringify(this.equips));
  }

}